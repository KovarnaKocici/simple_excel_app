﻿namespace simple_excel_app
{
    partial class RadioChoiceDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_OK = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.rgr_DeleteOptions = new System.Windows.Forms.GroupBox();
            this.rbtn_DeleteColumn = new System.Windows.Forms.RadioButton();
            this.rbtn_DeleteRow = new System.Windows.Forms.RadioButton();
            this.rgr_InsertOptions = new System.Windows.Forms.GroupBox();
            this.rbtn_InsertColumnRight = new System.Windows.Forms.RadioButton();
            this.rbtn_InsertColumnLeft = new System.Windows.Forms.RadioButton();
            this.rbtn_InsertRowBelow = new System.Windows.Forms.RadioButton();
            this.rbtn_InsertRowUp = new System.Windows.Forms.RadioButton();
            this.rgr_DeleteOptions.SuspendLayout();
            this.rgr_InsertOptions.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_OK
            // 
            this.btn_OK.Location = new System.Drawing.Point(37, 181);
            this.btn_OK.Name = "btn_OK";
            this.btn_OK.Size = new System.Drawing.Size(113, 43);
            this.btn_OK.TabIndex = 0;
            this.btn_OK.Text = "OK";
            this.btn_OK.UseVisualStyleBackColor = true;
            this.btn_OK.Click += new System.EventHandler(this.btn_OK_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(166, 181);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(113, 43);
            this.button2.TabIndex = 1;
            this.button2.Text = "Cancel";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // rgr_DeleteOptions
            // 
            this.rgr_DeleteOptions.Controls.Add(this.rbtn_DeleteColumn);
            this.rgr_DeleteOptions.Controls.Add(this.rbtn_DeleteRow);
            this.rgr_DeleteOptions.Location = new System.Drawing.Point(1, 11);
            this.rgr_DeleteOptions.Name = "rgr_DeleteOptions";
            this.rgr_DeleteOptions.Size = new System.Drawing.Size(278, 157);
            this.rgr_DeleteOptions.TabIndex = 2;
            this.rgr_DeleteOptions.TabStop = false;
            this.rgr_DeleteOptions.Text = "Delete";
            this.rgr_DeleteOptions.Visible = false;
            // 
            // rbtn_DeleteColumn
            // 
            this.rbtn_DeleteColumn.AutoSize = true;
            this.rbtn_DeleteColumn.Location = new System.Drawing.Point(11, 55);
            this.rbtn_DeleteColumn.Name = "rbtn_DeleteColumn";
            this.rbtn_DeleteColumn.Size = new System.Drawing.Size(85, 24);
            this.rbtn_DeleteColumn.TabIndex = 1;
            this.rbtn_DeleteColumn.Text = "column";
            this.rbtn_DeleteColumn.UseVisualStyleBackColor = true;
            // 
            // rbtn_DeleteRow
            // 
            this.rbtn_DeleteRow.AutoSize = true;
            this.rbtn_DeleteRow.Checked = true;
            this.rbtn_DeleteRow.Location = new System.Drawing.Point(11, 25);
            this.rbtn_DeleteRow.Name = "rbtn_DeleteRow";
            this.rbtn_DeleteRow.Size = new System.Drawing.Size(59, 24);
            this.rbtn_DeleteRow.TabIndex = 0;
            this.rbtn_DeleteRow.TabStop = true;
            this.rbtn_DeleteRow.Text = "row";
            this.rbtn_DeleteRow.UseVisualStyleBackColor = true;
            // 
            // rgr_InsertOptions
            // 
            this.rgr_InsertOptions.Controls.Add(this.rbtn_InsertColumnRight);
            this.rgr_InsertOptions.Controls.Add(this.rbtn_InsertColumnLeft);
            this.rgr_InsertOptions.Controls.Add(this.rbtn_InsertRowBelow);
            this.rgr_InsertOptions.Controls.Add(this.rbtn_InsertRowUp);
            this.rgr_InsertOptions.Location = new System.Drawing.Point(300, 11);
            this.rgr_InsertOptions.Name = "rgr_InsertOptions";
            this.rgr_InsertOptions.Size = new System.Drawing.Size(278, 157);
            this.rgr_InsertOptions.TabIndex = 4;
            this.rgr_InsertOptions.TabStop = false;
            this.rgr_InsertOptions.Text = "Insert";
            this.rgr_InsertOptions.Visible = false;
            // 
            // rbtn_InsertColumnRight
            // 
            this.rbtn_InsertColumnRight.AutoSize = true;
            this.rbtn_InsertColumnRight.Location = new System.Drawing.Point(11, 115);
            this.rbtn_InsertColumnRight.Name = "rbtn_InsertColumnRight";
            this.rbtn_InsertColumnRight.Size = new System.Drawing.Size(120, 24);
            this.rbtn_InsertColumnRight.TabIndex = 3;
            this.rbtn_InsertColumnRight.TabStop = true;
            this.rbtn_InsertColumnRight.Text = "column right";
            this.rbtn_InsertColumnRight.UseVisualStyleBackColor = true;
            // 
            // rbtn_InsertColumnLeft
            // 
            this.rbtn_InsertColumnLeft.AutoSize = true;
            this.rbtn_InsertColumnLeft.Location = new System.Drawing.Point(11, 85);
            this.rbtn_InsertColumnLeft.Name = "rbtn_InsertColumnLeft";
            this.rbtn_InsertColumnLeft.Size = new System.Drawing.Size(111, 24);
            this.rbtn_InsertColumnLeft.TabIndex = 2;
            this.rbtn_InsertColumnLeft.TabStop = true;
            this.rbtn_InsertColumnLeft.Text = "column left";
            this.rbtn_InsertColumnLeft.UseVisualStyleBackColor = true;
            // 
            // rbtn_InsertRowBelow
            // 
            this.rbtn_InsertRowBelow.AutoSize = true;
            this.rbtn_InsertRowBelow.Location = new System.Drawing.Point(11, 55);
            this.rbtn_InsertRowBelow.Name = "rbtn_InsertRowBelow";
            this.rbtn_InsertRowBelow.Size = new System.Drawing.Size(104, 24);
            this.rbtn_InsertRowBelow.TabIndex = 1;
            this.rbtn_InsertRowBelow.TabStop = true;
            this.rbtn_InsertRowBelow.Text = "row below";
            this.rbtn_InsertRowBelow.UseVisualStyleBackColor = true;
            // 
            // rbtn_InsertRowUp
            // 
            this.rbtn_InsertRowUp.AutoSize = true;
            this.rbtn_InsertRowUp.Checked = true;
            this.rbtn_InsertRowUp.Location = new System.Drawing.Point(11, 25);
            this.rbtn_InsertRowUp.Name = "rbtn_InsertRowUp";
            this.rbtn_InsertRowUp.Size = new System.Drawing.Size(81, 24);
            this.rbtn_InsertRowUp.TabIndex = 0;
            this.rbtn_InsertRowUp.TabStop = true;
            this.rbtn_InsertRowUp.Text = "row up";
            this.rbtn_InsertRowUp.UseVisualStyleBackColor = true;
            // 
            // RadioChoiceDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(312, 237);
            this.Controls.Add(this.rgr_InsertOptions);
            this.Controls.Add(this.rgr_DeleteOptions);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.btn_OK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "RadioChoiceDialog";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.RadioChoiceDialog_Load);
            this.rgr_DeleteOptions.ResumeLayout(false);
            this.rgr_DeleteOptions.PerformLayout();
            this.rgr_InsertOptions.ResumeLayout(false);
            this.rgr_InsertOptions.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_OK;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.GroupBox rgr_DeleteOptions;
        private System.Windows.Forms.RadioButton rbtn_DeleteColumn;
        private System.Windows.Forms.RadioButton rbtn_DeleteRow;
        private System.Windows.Forms.GroupBox rgr_InsertOptions;
        private System.Windows.Forms.RadioButton rbtn_InsertColumnRight;
        private System.Windows.Forms.RadioButton rbtn_InsertColumnLeft;
        private System.Windows.Forms.RadioButton rbtn_InsertRowBelow;
        private System.Windows.Forms.RadioButton rbtn_InsertRowUp;
    }
}