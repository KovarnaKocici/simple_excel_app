﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;


namespace simple_excel_app
{

    public partial class DataGridForm : Form
    {

        private const int initSize = 100;

        List<List<string>> FormulaTable = new List<List<string>>();
        List<List<List<string>>> Connection_List = new List<List<List<string>>>();

        private string FilePath = string.Empty;
        private bool IsOpened = false;
        private bool IsSaved = false;
        private bool IsClosed = true;

        //##################################################
        //##################### Events #####################
        //##################################################

        public DataGridForm()
        {
            InitializeComponent();

        }

        private void DataGridForm_Load(object sender, EventArgs e)
        {
            EditFileMode(false);
        }

        private void btn_New_Click(object sender, EventArgs e)
        {
            NewExcelFile();
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            try
            {
                OpenExcelFile();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                SaveExcelFile();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_Close_Click(object sender, EventArgs e)
        {
            CloseExcelFile();
        }

        private void dgv_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (!EmptyCell(dgv.CurrentCell.RowIndex, dgv.CurrentCell.ColumnIndex) &&
                    FormulaTable[dgv.CurrentCell.RowIndex][dgv.CurrentCell.ColumnIndex] != "")
                dgv.CurrentCell.Value = FormulaTable[dgv.CurrentCell.RowIndex][dgv.CurrentCell.ColumnIndex];
        }

        private void dgv_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (!EmptyCell(dgv.CurrentCell.RowIndex, dgv.CurrentCell.ColumnIndex))
                    InstallCurrentCellValue(dgv.CurrentCell.Value.ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgv_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (IsOpened && IsSaved) IsSaved = false;
        }

        private void dgv_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (dgv.CurrentCell.ColumnIndex != -1 && dgv.CurrentCell.RowIndex != -1)
            {
                string cellIndex = CellIndex(dgv.CurrentCell.ColumnIndex, dgv.CurrentCell.RowIndex);
                tbSelectedCell.Text = cellIndex;

                if (EmptyCell(dgv.CurrentCell.RowIndex, dgv.CurrentCell.ColumnIndex))
                    tbFormula.Text = "";
                else tbFormula.Text = FormulaTable[dgv.CurrentCell.RowIndex][dgv.CurrentCell.ColumnIndex].ToString();
            }
        }

        private void Fx_Click(object sender, EventArgs e)
        {

        }

        private void tbFormula_TextChanged(object sender, EventArgs e)
        {
                dgv.CurrentCell.Value = tbFormula.Text;
        }

        private void tbFormula_Leave(object sender, EventArgs e)
        {
            try
            {
                InstallCurrentCellValue(tbFormula.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgv_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.ColumnIndex != -1 && e.RowIndex != -1 && e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                DataGridViewCell c = (sender as DataGridView)[e.ColumnIndex, e.RowIndex];
                if (!c.Selected)
                {
                    c.DataGridView.ClearSelection();
                    c.DataGridView.CurrentCell = c;
                    c.Selected = true;
                }
                if (e.Button == MouseButtons.Right) contextMenuCell.Show(Cursor.Position);
            }
        }

        private void DataGridForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!CloseExcelFile()) e.Cancel = true;
        }

        private void insertToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                switch (RadioChoiceWindow("Insert"))
                {
                    case "row up":
                        NewRow(dgv.CurrentCell.RowIndex);
                        break;
                    case "row below":
                        NewRow(dgv.CurrentCell.RowIndex + 1);
                        break;
                    case "column left":
                        NewColumn(dgv.CurrentCell.ColumnIndex);
                        break;
                    case "column right":
                        NewColumn(dgv.CurrentCell.ColumnIndex + 1);
                        break;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                switch (RadioChoiceWindow("Delete"))
                {
                    case "row":
                        DeleteRow();
                        InstallRowIndex();
                        break;

                    case "column":
                        DeleteColumn();
                        InstallColumnName();
                        break;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void dgv_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                if (dgv.Rows[e.RowIndex].Cells[e.ColumnIndex].Selected)
                {
                    e.Paint(e.CellBounds, DataGridViewPaintParts.All & ~DataGridViewPaintParts.Border);
                    using (Pen p = new Pen(Color.FromArgb(33, 115, 70), 1))
                    {
                        Rectangle rect = e.CellBounds;
                        rect.Width -= 2;
                        rect.Height -= 2;

                        using (Font font = new Font("Microsoft Sans Serif", 8F, FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204))))
                        {
                            using (StringFormat sf = new StringFormat())
                            {

                                sf.LineAlignment = StringAlignment.Center;
                                sf.Alignment = StringAlignment.Near;
                                e.Graphics.DrawString((String)e.Value, dgv.Font, Brushes.Black, rect, sf);
                                e.Graphics.DrawRectangle(p, rect);

                            }
                        }
                    }
                    e.Handled = true;
                }
            }
        }

        //#####################################################
        //################## Launch Settings ##################
        //#####################################################

        private void EditFileMode(bool Prp)
        {
            //tools
            if (Prp)
            {
                tbSelectedCell.Text = "";
                tbFormula.Text = "";
            }
            tbSelectedCell.Enabled = Prp;
            tbFormula.Enabled = Prp;
            //buttons
            btnSave.Enabled = Prp;
            btn_Close.Enabled = Prp;
        }

        //##################################################
        //################## DGV Elements ##################
        //##################################################

        private void NewRow(int Index)
        {
            //Add row
            DataGridViewRow IRow = new DataGridViewRow();
            IRow.Height = dgv.Rows[dgv.CurrentCell.RowIndex].Height;
            dgv.Rows.Insert(Index, IRow);
            InstallRowIndex();

            List<string> IList = new List<string>();
            for (int i = 0; i < dgv.ColumnCount - 1; i++)
                IList.Add(string.Empty);
            FormulaTable.Insert(Index, IList);

            List<List<string>> IIRTList = new List<List<string>>();
            for (int i = 0; i < dgv.ColumnCount - 1; i++)
                IIRTList.Add(new List<string>());
            Connection_List.Insert(Index, IIRTList);

            //Clear IsReferredTo_List
            for (int row = 0; row < dgv.RowCount - 1; row++)
                for (int column = 0; column < dgv.ColumnCount - 1; column++)
                    Connection_List[row][column].Clear();

            //Refresh Values
            for (int row = 0; row < dgv.RowCount - 1; row++)
                for (int column = 0; column < dgv.ColumnCount - 1; column++)
                    dgv[column, row].Value = Calc(FormulaTable[row][column], row, column);
        }

        private void NewColumn(int Index)
        {
            //Add Column
            DataGridViewColumn IColumn = new DataGridViewTextBoxColumn();
            dgv.Columns.Insert(Index, IColumn);
            InstallColumnName();

            for (int i = 0; i < dgv.RowCount - 1; i++)
                FormulaTable[i].Insert(Index, string.Empty);

            for (int i = 0; i < dgv.RowCount - 1; i++)
                Connection_List[i].Insert(Index, new List<string>());

            //Clear IsReferredTo_List
            for (int row = 0; row < dgv.RowCount - 1; row++)
                for (int column = 0; column < dgv.ColumnCount - 1; column++)
                    Connection_List[row][column].Clear();

            //Refresh Values
            for (int row = 0; row < dgv.RowCount - 1; row++)
                for (int column = 0; column < dgv.ColumnCount - 1; column++)
                    dgv[column, row].Value = Calc(FormulaTable[row][column], row, column);
        }

        private void DeleteRow()
        {
            //Remove row from Connection_List
            Connection_List.RemoveAt(dgv.CurrentCell.RowIndex);

            //Remove row from DGV
            dgv.Rows.RemoveAt(dgv.CurrentCell.RowIndex);

            //Remove row from FormulaTable
            FormulaTable.RemoveAt(dgv.CurrentCell.RowIndex);

            //Clear IsReferredTo_List
            for (int row = 0; row < dgv.RowCount - 1; row++)
                for (int column = 0; column < dgv.ColumnCount - 1; column++)
                    Connection_List[row][column].Clear();

            //Refresh Values
            for (int row = 0; row < dgv.RowCount - 1; row++)
                for (int column = 0; column < dgv.ColumnCount - 1; column++)
                    dgv[column, row].Value = Calc(FormulaTable[row][column], row, column);
        }


        private void DeleteColumn()
        { 
            //Remove column from Connection_List
            for (int row = 0; row < dgv.RowCount - 1; row++)
                Connection_List[row].RemoveAt(dgv.CurrentCell.ColumnIndex);

            //Remove column from DGV
            dgv.Columns.RemoveAt(dgv.CurrentCell.ColumnIndex);

            //Remove column from FormulaTable
            for (int row = 0; row < dgv.RowCount - 1; row++)
                FormulaTable[row].RemoveAt(dgv.CurrentCell.ColumnIndex);

            //Clear IsReferredTo_List
            for (int row = 0; row < dgv.RowCount - 1; row++)
                for (int column = 0; column < dgv.ColumnCount - 1; column++)
                    Connection_List[row][column].Clear();

            //Refresh Values
            for (int row = 0; row < dgv.RowCount - 1; row++)
                for (int column = 0; column < dgv.ColumnCount - 1; column++)
                    dgv[column, row].Value = Calc(FormulaTable[row][column], row, column);
        }

        private void InstallColumnName()
        {
            for (int i = 0; i < dgv.ColumnCount; i++)
                dgv.Columns[i].HeaderText = ColumnNameFromIndex(i);
        }

        private void InstallRowIndex()
        {
            for (int i = 0; i < dgv.Rows.Count; i++)
                dgv.Rows[i].HeaderCell.Value = Convert.ToString(i + 1);
        }

        private string CellIndex(int column, int row)
        {
            return dgv.Columns[column].HeaderText + Convert.ToString(row + 1);
        }

        private string[] ParsedCellIndex(string CellIndex)
        {
            string ColumnName = string.Empty;
            for (int i = 0; i < CellIndex.Length; i++) if (Char.IsLetter(CellIndex[i])) ColumnName += CellIndex[i];
            string RowIndex = string.Empty;
            for (int i = 0; i < CellIndex.Length; i++) if (Char.IsDigit(CellIndex[i])) RowIndex += CellIndex[i];

            return new string[] { ColumnName, RowIndex };
        }

        private void InstallCurrentCellValue(string value)
        {
            FormulaTable[dgv.CurrentCell.RowIndex][dgv.CurrentCell.ColumnIndex] = value;
            dgv.CurrentCell.Value = Calc(FormulaTable[dgv.CurrentCell.RowIndex][dgv.CurrentCell.ColumnIndex], dgv.CurrentCell.RowIndex, dgv.CurrentCell.ColumnIndex);
            RefreshConnectedCellValue(dgv.CurrentCell.RowIndex, dgv.CurrentCell.ColumnIndex);
        }

        private void RefreshConnectedCellValue(int RowIndex, int ColumnIndex)
        {
                for (int i = 0; i < Connection_List[RowIndex][ColumnIndex].Count(); i++)
                {
                    string[] CellName = ParsedCellIndex(Connection_List[RowIndex][ColumnIndex][i]);
                    if (!EmptyCell(Int32.Parse(CellName[1]) - 1, IndexFromColumnName(CellName[0])))
                        dgv[IndexFromColumnName(CellName[0]), Int32.Parse(CellName[1]) - 1].Value =
                            Calc(FormulaTable[Int32.Parse(CellName[1]) - 1][IndexFromColumnName(CellName[0])],
                                Int32.Parse(CellName[1]) - 1, IndexFromColumnName(CellName[0]));
                    RefreshConnectedCellValue(Int32.Parse(CellName[1]) - 1, IndexFromColumnName(CellName[0]));
                }
        }

        private bool EmptyCell(int i, int j)
        {
            if (dgv[j, i].Value == null ||
                    dgv[j, i].Value == DBNull.Value ||
                    String.IsNullOrWhiteSpace(dgv[j, i].Value.ToString())) return true;
            return false;
        }

        public string ColumnNameFromIndex(int column)
        {
            string columnString = "";
            int columnNumber = column;
            char currentLetter;

            while (true)
            {
                currentLetter = (char)((columnNumber % 26) + 65);
                columnNumber = columnNumber / 26;
                columnString = currentLetter + columnString;
                if (columnNumber > 0) columnNumber--;
                else if (columnNumber == 0) break;
            }

            return columnString;
        }

        public int IndexFromColumnName(string column)//refact
        {
            int retVal = 0;
            string col = column.ToUpper();
            for (int iChar = col.Length - 1; iChar >= 0; iChar--)
            {
                char colPiece = col[iChar];
                int colNum = colPiece - 64;
                retVal = retVal + colNum * (int)Math.Pow(26, col.Length - (iChar + 1));
            }
            return retVal - 1;
        }

        //########################################################
        //################## Additional Windows ##################
        //########################################################

        private bool CallFileDialog(ref FileDialog Dialog)
        {

            Dialog.Filter = "Excel files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
            if (Dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK) return true;
            return false;

        }

        private int WarningWindow(string message)
        {
            DialogResult answer = MessageBox.Show(message, "Simple Excel App",
                MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);

            switch (answer)
            {
                case DialogResult.Yes: return 1;
                case DialogResult.No: return -1;
                case DialogResult.Cancel: return 0;
            }
            return 0;
        }

        private string RadioChoiceWindow(string Type)
        {
            string opt = string.Empty;
            using (var ConcreteRadioChoiceDialog = new RadioChoiceDialog(Type))
            {

                var result = ConcreteRadioChoiceDialog.ShowDialog();
                if (result == DialogResult.OK)
                {
                    opt = ConcreteRadioChoiceDialog.Option;
                }
            }
            return opt;
        }

        //##########################################
        //############### Excel Book ###############
        //##########################################

        private void NewExcelFile()
        {
            if (!IsClosed) CloseExcelFile();
            ExcelBook_Load("");
            IsOpened = true;
            IsClosed = false;
        }

        private void OpenExcelFile()
        {
            try
            {
                if (!IsClosed && CloseExcelFile() || IsClosed)
                {
                    BindingExcel Excel = new BindingExcel();
                    FileDialog openFile = new OpenFileDialog();
                    if (CallFileDialog(ref openFile))
                    {

                        FilePath = openFile.FileName;
                        Excel.OpenExcelFile(openFile.FileName);
                        ExcelBook_Load(openFile.FileName);
                        IsSaved = true;
                        ImportExcelFile(Excel);
                        IsOpened = true;
                    }
                    Excel.Close();
                    IsClosed = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ExcelBook_Load(string Path)
        {
            FilePath = Path;
            //Create DataGridView
            for (int i = 0; i < initSize + 1; i++) dgv.Columns.Add(ColumnNameFromIndex(i), ColumnNameFromIndex(i));
            for (int i = 0; i < initSize; i++)
                dgv.Rows.Add();

            //Initialize 2D dym=namic array for the formulas
            for (int i = 0; i < initSize; i++)
                FormulaTable.Add(new List<string>());

            for (int i = 0; i<initSize; i++)
                for (int j = 0; j < initSize; j++) FormulaTable[i].Add(string.Empty);

            //Initialize Connection List for each cell
            for (int i = 0; i < initSize; i++)
                Connection_List.Add(new List<List<string>>());

            for (int i = 0; i < initSize; i++)
                for (int j = 0; j < initSize; j++) Connection_List[i].Add(new List<string>());

            // Decoration
            InstallRowIndex();

            EditFileMode(true);
        }
        
        private void ImportExcelFile(BindingExcel App)
        {

            int rowCount = App.xlRange.Row;
            int colCount = App.xlRange.Column;

            //fill table with the data from the excel file 
            //excel is not zero based
            for (int i = 1; i <= rowCount; i++)
            {
                for (int j = 1; j <= colCount; j++)
                {

                    if (App.xlWorksheet.Cells[i, j] != null && App.xlWorksheet.Cells[i, j].Value != null)
                        dgv[j - 1, i - 1].Value = Calc(App.xlWorksheet.Cells[i, j].Formula, j-1, i-1);
                    FormulaTable[i - 1][j- 1] = App.xlWorksheet.Cells[i, j].Formula;
                }
            }
        }

        private void ExportExcelFile(BindingExcel App)
        {

            int rowCount = dgv.Rows.Count;
            int colCount = dgv.Columns.Count-1;

            for (int i = 0; i < rowCount; i++)
            {
                for (int j = 0; j < colCount; j++)
                {
                    if (!EmptyCell(i, j))
                        App.xlWorksheet.Cells[i + 1, j + 1] = dgv[j, i].Value.ToString();
                    App.xlWorksheet.Cells[i + 1, j + 1].Formula = FormulaTable[i][j];
                }
            }

        }

        public void SaveExcelFile()
        {
            BindingExcel Excel = new BindingExcel();
            FileDialog saveFile = new SaveFileDialog();
            if (CallFileDialog(ref saveFile))
            {
                ExportExcelFile(Excel);
                Excel.xlWorkbook.SaveAs(saveFile.FileName);
                IsSaved = true;
            }
            Excel.Close();
        }

        private bool CloseExcelFile()
        {
            if (IsOpened && !IsSaved)
            {
                int answer;
                answer = WarningWindow("Do you want to save the changes you made to \"" + FilePath + "\"?");
                switch (answer)
                {
                    case 1:
                        SaveExcelFile();
                        if (IsSaved) Clear();
                        else return false;
                        break;
                    case -1:
                        Clear();
                        break;
                    case 0:
                        return false;
                }
            }
            else Clear();
            IsClosed = true;
            return true;
        }

        private void Clear()
        {
            dgv.Columns.Clear();
            FormulaTable.Clear();
            EditFileMode(false);
            IsClosed = true;
            IsOpened = false;
        }

        //##############################################
        //################## Formulas ##################
        //##############################################

        private string ConvertToNumExpr(string input, string[] ListOfOperators, int CurrCellRowIndex, int CurrCellColumnIndex)
        {
            string Expr = string.Empty;
            int pos = 0;

            while (pos < input.Length)
            {
                string s = string.Empty + input[pos];
                if (!ListOfOperators.Contains(input[pos].ToString()))
                {
                    if (Char.IsDigit(input[pos]))
                        for (pos += 1; pos < input.Length &&
                            (Char.IsDigit(input[pos]) || input[pos] == ',' || input[pos] == '.'); pos++)
                            s += input[pos];
                    else if (Char.IsLetter(input[pos]))
                    {
                        for (pos += 1; pos < input.Length &&
                            (Char.IsLetter(input[pos]) || Char.IsDigit(input[pos])); pos++)
                            s += input[pos];
                        if (!ListOfOperators.Contains(s))
                        {
                            //check recursion
                            if (s==CellIndex(CurrCellColumnIndex, CurrCellRowIndex) || Connection_List[CurrCellRowIndex][CurrCellColumnIndex].Contains(s))
                                throw new InvalidOperationException
                                    ("There are one or more circular references where a formula refers to its own cell either directly or idirectly. This might cause them to calculate incorrectly");

                            string[] cellName= ParsedCellIndex(s);
                            if (IndexFromColumnName(cellName[0]) < dgv.ColumnCount && Int32.Parse(cellName[1])-1 < dgv.RowCount)
                            {

                                //Add to IsReferred_List
                                if (!Connection_List[Int32.Parse(cellName[1]) - 1][IndexFromColumnName(cellName[0])]
                                    .Contains(CellIndex(CurrCellColumnIndex, CurrCellRowIndex)))

                                    Connection_List[Int32.Parse(cellName[1]) - 1][IndexFromColumnName(cellName[0])]
                                        .Add(CellIndex(CurrCellColumnIndex, CurrCellRowIndex));

                                if (EmptyCell(Int32.Parse(cellName[1]) - 1, IndexFromColumnName(cellName[0])))
                                    s = "0";
                                else s = dgv[IndexFromColumnName(cellName[0]), Int32.Parse(cellName[1]) - 1].Value.ToString();
                            }

                        }
                    }
                }
                else pos++;
                Expr += s;
            }
            return Expr;
        }

        private string Calc(string formula, int CurrCellRowIndex, int CurrCellColumnIndex)
        {
            string formatFormula = formula.ToUpper();
            foreach (char c in formatFormula) formatFormula.Replace(" ", string.Empty);

            if (formatFormula.StartsWith("="))
            {

                PostfixNotationExpression res = new PostfixNotationExpression();
                string NumExpr = ConvertToNumExpr(formatFormula.Substring(1), res.Operators.ToArray(), CurrCellRowIndex, CurrCellColumnIndex);
                return res.result(NumExpr).ToString();
            }
            return formula;
        }

    }

}
