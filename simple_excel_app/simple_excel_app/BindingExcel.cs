﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Excel = Microsoft.Office.Interop.Excel;

using System.IO;
using System.Runtime.InteropServices;
using Microsoft.Win32.SafeHandles;
using System.Diagnostics;
using System.Collections;

namespace simple_excel_app
{

    class BindingExcel:IDisposable
    {
        private Excel.Application _xlApp;
        public Excel.Application XlApp
        {
            get
            { return _xlApp; }
            set
            {
                
                //check whether Excel is installed in your system
                if (value == null) throw new ApplicationException("Excel is not properly installed");
                else _xlApp = value;
             
            }
        }

        private Excel.Workbooks _OWorkbooks;
        private Excel.Workbook _xlWorkbook;
        public Excel.Workbook xlWorkbook { get { return _xlWorkbook; } }
        private Excel.Sheets _xlSheets;
        private Excel.Worksheet _xlWorksheet;
        public Excel.Worksheet xlWorksheet { get { return _xlWorksheet; } }
        private Excel.Range _xlRange;
        public Excel.Range xlRange { get { return _xlRange; } }

        public BindingExcel()
        {
            
            //initialize the Excel application Object
            XlApp = new Microsoft.Office.Interop.Excel.Application();

           _OWorkbooks = _xlApp.Workbooks;
           _xlWorkbook = _OWorkbooks.Add();
           _xlSheets = _xlWorkbook.Worksheets;
           _xlWorksheet = (Excel.Worksheet)(_xlSheets[1]);
           _xlRange = null;
        }
       
        public void OpenExcelFile(string FilePath)
        {
            if (!string.IsNullOrEmpty(FilePath) && File.Exists(FilePath))
            {
                _xlWorkbook = _OWorkbooks.Open(FilePath, ReadOnly: false);
                _xlSheets = _xlWorkbook.Worksheets;
                _xlWorksheet = (Excel.Worksheet)(_xlSheets[1]);
                _xlRange = xlWorksheet.get_Range("A1").SpecialCells(Microsoft.Office.Interop.Excel.XlCellType.xlCellTypeLastCell);
            }
            else { throw new FileNotFoundException("File wasn`t found"); }
        }

        private void releaseObject( object obj)
        {
            try
            {
                if (obj != null)
                {
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(obj);
                    obj = null;
                }
            }
            catch (Exception ex)
            {
                obj = null;
                throw new ApplicationException("Unable to release COM Object", ex);
            }
        }

        private void CleanUp()
        {
            //close and quit
            _xlWorkbook.Close(true);
            _xlApp.Quit();

            //release com objects to fully kill excel process from running in the background
            releaseObject(_xlRange);
            releaseObject(_xlWorksheet);
            releaseObject(_xlSheets);
            releaseObject(_xlWorkbook);
            releaseObject(_OWorkbooks);
            releaseObject(_xlApp);

            //cleanup
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        private bool disposed = false;

        public void Close()
        {
            Dispose(true);//dispose unmanaged resources
            GC.SuppressFinalize(this);//finalizer not running
        }
        public void Dispose()
        {
            Close();
        }
        private void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                CleanUp();//managed objects
            }

            disposed = true;
 
        }

        ~BindingExcel()
        {
            Dispose(false);
        }
    }
}

