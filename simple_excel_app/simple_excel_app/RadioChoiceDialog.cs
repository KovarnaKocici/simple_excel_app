﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace simple_excel_app
{
    public partial class RadioChoiceDialog : Form
    {

        private string _type; 
        public string Type { get { return _type; } }

        private string _option;
        public string Option { get { return _option; } }


        public RadioChoiceDialog(string Type)
        {
            InitializeComponent();

            CancelButton = button2;
            AcceptButton = btn_OK;

            _type = Type;
        }
       
        private void RadioChoiceDialog_Load(object sender, EventArgs e)
        {
            
            Left = Cursor.Position.X;
            Top = Cursor.Position.Y;

            if (Type == "Insert") {
                this.Text = "Insert";
                rgr_InsertOptions.Location = new Point(17, 12);
                rgr_InsertOptions.Visible = true;
            }
            else if (Type == "Delete") {
                this.Text = "Delete";
                rgr_DeleteOptions.Location = new Point(17, 12);
                rgr_DeleteOptions.Visible = true;
            }
            else throw new NotSupportedException("Invalid type");
        }

        private void btn_OK_Click(object sender, EventArgs e)
        {
            if (Type == "Delete") CheckedOption(this.rgr_DeleteOptions);
            else CheckedOption(this.rgr_InsertOptions);
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void CheckedOption(GroupBox Group)
        {
            foreach (Control control in Group.Controls)
            {
                if (control is RadioButton)
                {
                    RadioButton radio = control as RadioButton;
                    if (radio.Checked)
                    {
                        _option = radio.Text;
                    }
                }
            }
        }

    }
}
